<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../repository/UserRepository.php';

session_start();

class SecurityController extends AppController
{
    public function loginSC(){
        $userRepository = new UserRepository();

        if(!$this->isPost()){
            $this->render('login');
            return;
        }

        $username = $_POST["username"];
        $password = $_POST["password"];

        $username = htmlentities($username, ENT_QUOTES);
        $password = htmlentities($password, ENT_QUOTES);

        try {
            $user = $userRepository->getUser($username);
        }
        catch (Exception $e)
        {
            $this->render('login', ["messages" => [$e->getMessage()]]);
            return;
        }

        if(($user->getUsername() != $username) || (!password_verify($user->getPasswordSalt().$password, $user->getPassword()))){
            $this->render('login', ['messages' => ["Wrong username or password"]]);
            return;
        }

        //Check if user is an admin
        if($userRepository->checkIfUserIsAnAdmin($user->getUserID())){
            $_SESSION['admin'] = true;
        }

        $_SESSION['loggedIn'] = true;
        $_SESSION['user_id'] = $user->getUserID();
        $_SESSION['username'] = $user->getUsername();
        try {
            $_SESSION['avatar'] = $userRepository->getUserAvatar($_SESSION['user_id']);
        }
        catch (PDOException $e){
            $_SESSION['avatar'] = "none.png";
        }
        try{
            $_SESSION['food_type'] = $userRepository->getUserFoodType($_SESSION['user_id']);
        }
        catch (PDOException $e){
            $_SESSION['food_type'] = "Error! not defined";
        }

        $this->render('recipesPage');
    }

    public function logoutSC(){
        session_unset();
        $this->render('login');
    }

    public function registerSC(){
        $userRepository = new UserRepository();

        if(!$this->isPost()){
            $this->render('register');
        }

        $username = $_POST["username"];
        $name = $_POST["name"];
        $surname = $_POST["surname"];
        $email = $_POST["email"];
        $password = $_POST["password"];
        $repeatPassword = $_POST["repeat-password"];
        $foodType = $_POST["food-type"];

        $errorMessages = [];

        //Username check
        if((strlen($username) < 5) || (strlen($username) > 24)) {
            array_push($errorMessages, ['userE' => ["Username should have between 5 - 24 characters"]]);
        }
        else if(ctype_alnum($username) == false){
            array_push($errorMessages, ['userE' => ["Username should only consist of english characters or numbers"]]);
        }
        else if($userRepository->checkIfUserExists($username)){
            array_push($errorMessages, ['userE' => ["Account with this username already exists"]]);
        }
        //---------------

        //Email check
        $filteredEmail = filter_var($email, FILTER_SANITIZE_EMAIL);
        if((filter_var($email, FILTER_VALIDATE_EMAIL) == false) || ($email != $filteredEmail)){
            array_push($errorMessages, ['emailE' => ["Email format is not correct"]]);
        }
        else if(strlen($email) > 255){
            array_push($errorMessages, ['emailE' => ["Email excited the 255 character limit"]]);
        }
        else if($userRepository->checkIfEmailExists($email) == true){
            array_push($errorMessages, ['emailE' => ["Account with this email already exists"]]);
        }
        //---------------

        //Name check
        if(strlen($name) < 1 || strlen($name) > 255){
            array_push($errorMessages, ['nameE' => ["Name should have between 1 - 255 characters"]]);
        }
        else if(ctype_alpha($name) == false){
            array_push($errorMessages, ['nameE' => ["Name should only consist of english characters"]]);
        }
        //---------------

        //Surname check
        if(strlen($surname) < 1 || strlen($surname) > 255){
            array_push($errorMessages, ['surnameE' => ["Surname should have between 1 - 255 characters"]]);
        }
        else if(ctype_alpha($surname) == false){
            array_push($errorMessages, ['surnameE' => ["Surname should only consist of english characters"]]);
        }
        //---------------

        //Password check
        if(strlen($password) < 8 || strlen($password) > 32){
            array_push($errorMessages, ['passwordE' => ["Password should have between 8 - 32 characters"]]);
        }
        else if(preg_match('/[A-Z]/' ,$password) == false ||
                preg_match('/[a-z]/' ,$password) == false ||
                preg_match('/[0-9]/' ,$password) == false){
            array_push($errorMessages, ['passwordE' => ["Password should contain at lest:<br/>1 small letter<br/>1 big letter<br/>1 number"]]);
        }
        if($password != $repeatPassword){
            array_push($errorMessages, ['passwordRE' => ["Provided passwords are different"]]);
        }
        //---------------

        //FoodType check
        if(empty($foodType)){
            array_push($errorMessages, ['foodTypeE' => ["Pick preferred food type"]]);
        }
        //---------------

        //Captcha check
        $GJsonResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.BOT_PRIVATE_KEY.'&response='.$_POST['g-recaptcha-response']);
        $GResponse = json_decode($GJsonResponse);

        if($GResponse->success == false){
            array_push($errorMessages, ['captchaE' => ["Captcha test failed"]]);
        }
        //---------------

        if(count($errorMessages) > 0){
            $this->render('register', $errorMessages);
            return;
        }

        //All values are correct
        $userRepository->createUser($username, $name, $surname, $email, $password, $foodType);

        try {
            $user_id = $userRepository->getUserId($username);
            $_SESSION['loggedIn'] = true;
            $_SESSION['user_id'] = $user_id;
            $_SESSION['username'] = $username;
            $_SESSION['avatar'] = $userRepository->getUserAvatar($user_id);
            $_SESSION['food_type'] = $userRepository->getUserFoodType($user_id);
        }
        catch (Exception $e)
        {
            $this->render('login', ["messages" => [$e->getMessage()]]);
            return;
        }
        $this->render('recipesPage');

    }
}