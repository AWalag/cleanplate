<?php

require_once 'AppController.php';
require_once __DIR__.'/../repository/FavoriteRepository.php';

class FavoriteController extends AppController{
    public function addToFavorite(){
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

        if($contentType === "application/json"){
            $content = trim(file_get_contents("php://input"));
            $decoded = json_decode($content, true);

            $favoriteRepository = new FavoriteRepository();
            $favoriteRepository->addRecipeToFavorite($decoded['recipeId']);
        }
    }
}