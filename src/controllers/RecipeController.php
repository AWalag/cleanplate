<?php

require_once 'AppController.php';
require_once __DIR__.'/../repository/RecipeRepository.php';
require_once __DIR__.'/../repository/UnitRepository.php';

session_start();

class RecipeController extends AppController{
    const MAX_FILE_SIZE = 1024 * 1024;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    public const UPLOAD_DIRECTORY = '/../images/uploads/dishes/';
    const fields = ['dish_name', 'chef_name', 'recipe', 'products'];

    public function manageRecipes(){
        if(!$this->isPost()) {
            die("not post");
        }

        $operation = $_POST["manage_operation"];
        if(empty($operation)){
            $errorMessages = [['recipeOperationE' => ["Value not set"]]];
            $this->render('managePage', $errorMessages);
            return;
        }
        $operationStatus = false;
        switch ($operation){
            case "Add":
                $operationStatus = $this->addRecipe();
                break;
            case "Delete":
                $operationStatus = $this->deleteRecipe();
                break;
            default:
                die("operation not set");
                break;
        }
        if($operationStatus)
            $this->render("managePage");
    }

    private function addRecipe() : bool{
        $errorMessages = [];
        $recipeRepository = new RecipeRepository();

        $fieldValues = $this->validateFields($errorMessages);

        if($recipeRepository->checkIfRecipeExists($fieldValues[0])){
            array_push($errorMessages, ['dish_nameE' => ["This dish already exists change operation to update to change details"]]);
        }

        $fileError = "";
        if(!$this->isPost() or !is_uploaded_file($_FILES['dish_photo']['tmp_name'])) {
            array_push($errorMessages, ['photoE' => ["No file selected"]]);
        }

        else if($this->validatePhoto($_FILES['dish_photo'], $fileError) == false){
            array_push($errorMessages, ['photoE' => [$fileError]]);
        }

        if(empty($_POST['food_type'])){
            array_push($errorMessages, ['foodTypeE' => ["Field is empty"]]);
        }

        array_push($fieldValues, $_POST['food_type']);

        if(count($errorMessages) > 0){
            $this->render('managePage', $errorMessages);
            return false;
        }

        $shortType = $_FILES['dish_photo']['type'];
        $shortType = explode('/', $shortType)[1];
        $filePreName = str_replace(" ", "_", $fieldValues[0]);
        $fileName = $filePreName."_photo.".$shortType;
        move_uploaded_file(
            $_FILES['dish_photo']['tmp_name'],
            dirname(__DIR__).self::UPLOAD_DIRECTORY.$fileName
        );
        array_push($fieldValues, $fileName);

        $recipeRepository->createRecipe($fieldValues);

        return true;
    }

    private function deleteRecipe() : bool{
        $errorMessages = [];
        $recipeRepository = new RecipeRepository();

        if(empty($_POST['dish_name'])){
            array_push($errorMessages, ['dish_nameE' => ["Field is empty"]]);
        }
        $dish_name = $_POST['dish_name'];
        if($recipeRepository->checkIfRecipeExists($dish_name) == false){
            array_push($errorMessages, ['dish_nameE' => ["This dish does not exist in the database"]]);
        }
        if(count($errorMessages) > 0){
            $this->render('managePage', $errorMessages);
            return false;
        }

        $recipeRepository->deleteRecipe($dish_name);

        return true;
    }

    private function validateFields(&$errorMessages){
        $fieldValues = [];

        foreach (self::fields as $field){
            if(empty($_POST[$field])){
                array_push($errorMessages, [$field.'E' => ["Field is empty"]]);
            }
            if($field == "chef_name"){
                $name = explode(" ", $_POST[$field]);
                if(count($name) != 2){
                    array_push($errorMessages, ['chef_nameE' => ["Chef name and surname should be separated with a space"]]);
                }
            }
            else if($field == "products"){
                $this->validateProductsUnits($_POST[$field], $errorMessages);
            }

            $value = $_POST[$field];
            array_push($fieldValues, $value);
        }
        return $fieldValues;
    }

    private function validatePhoto(array $file, string &$error) : bool
    {
        if($file['size'] > self::MAX_FILE_SIZE){
            $error = "File is to large. Max size 1 mb";
            return false;
        }

        if(!isset($file['type']) or !in_array($file['type'], self::SUPPORTED_TYPES)){
            $error = "Only png and jpeg file formats allowed";
            return false;
        }
        return true;
    }

    private function validateProductsUnits(string $products, &$errorMessages) : bool{
        $units = new UnitRepository();
        $unitsList = $units->getUnitList();

        $products = explode("\n", $products);
        $productUnits = [];
        foreach ($products as $product){
            $unit = explode('[', $product)[3];
            $unit = substr_replace($unit ,"",-1);
            if($unit[-1] == ']')
                $unit = substr_replace($unit ,"",-1);
            array_push($productUnits, $unit);
        }

        foreach ($productUnits as $unit){
            if(in_array($unit, $unitsList) == false){
                array_push($errorMessages, ['productsE' => [$unit." unit does not exist in the database"]]);
                return false;
            }
        }

        return true;
    }
}