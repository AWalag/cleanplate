<?php

require_once 'AppController.php';
require_once __DIR__.'/../repository/ProductRepository.php';

session_start();

class ProductController extends AppController{
    private const fields = ['kcal', 'fats', 'proteins', 'sugars', 'carbs'];

    public function manageProducts(){

        if(!$this->isPost()) {
            die("not post");
        }

        $operation = $_POST["manage_operation"];
        if(empty($operation)){
            $errorMessages = [['$productOperationE' => ["Value not set"]]];
            $this->render('managePage', $errorMessages);
            return;
        }

        $operationStatus = false;
        switch ($operation){
            case "Add":
                $operationStatus = $this->addProduct();
                break;
            case "Update":
                $operationStatus = $this->updateProduct();
                break;
            case "Delete":
                $operationStatus = $this->deleteProduct();
                break;
            default:
                die("operation not set");
                break;
        }

        if($operationStatus)
            $this->render("managePage");
    }

    private function addProduct() : bool{
        $errorMessages = [];
        $productRepository = new ProductRepository();

        $product_name = $this->validateNameField($errorMessages);
        if($productRepository->checkIfProductExists($product_name)){
            array_push($errorMessages, ['nameE' => [$product_name." is in the database. Set operation to update"]]);
        }

        $fieldValues = $this->validateIntFields($errorMessages);

        if(count($errorMessages) > 0){
            $this->render('managePage', $errorMessages);
            return false;
        }

        $productRepository->createProduct($product_name, $fieldValues);
        return true;
    }

    private function updateProduct() : bool{
        $errorMessages = [];
        $productRepository = new ProductRepository();
        $product_name = $this->validateNameField($errorMessages);
        if(!$productRepository->checkIfProductExists($product_name)){
            array_push($errorMessages, ['nameE' => [$product_name." is not in the database. Set operation to Add"]]);
        }

        $fieldValues = $this->validateIntFields($errorMessages);

        if(count($errorMessages) > 0){
            $this->render('managePage', $errorMessages);
            return false;
        }

        $productRepository->updateProduct($product_name, $fieldValues);
        return true;
    }

    private function deleteProduct(){
        $errorMessages = [];
        $productRepository = new ProductRepository();
        $product_name = $this->validateNameField($errorMessages);
        if(!$productRepository->checkIfProductExists($product_name)){
            array_push($errorMessages, ['nameE' => [$product_name." is not in the database. Set operation to Add"]]);
        }

        if(count($errorMessages) > 0){
            $this->render('managePage', $errorMessages);
            return false;
        }

        $productRepository->deleteProduct($product_name);
        return true;
    }

    private function validateNameField(&$errorMessages){
        //Nazwa produktu
        if(!isset($_POST['product_name'])){
            array_push($errorMessages, ['nameE' => ["Value not set"]]);
        }

        $product_name = $_POST['product_name'];
        $product_name = strtolower($product_name);
        if(empty($product_name)){
            array_push($errorMessages, ['nameE' => ["Field is empty"]]);
        }
        //-------------------------

        return $product_name;
    }

    private function validateIntFields(&$errorMessages){
        $fieldValues = [];

        foreach (self::fields as $field ){
            if(empty($_POST[$field])){
                array_push($errorMessages, [$field.'E' => ["Field is empty"]]);
            }

            $value = $_POST[$field];
            if($value < 0){
                array_push($errorMessages, [$field.'E' => ["Value cannot be negative"]]);
            }
            array_push($fieldValues, $value);
        }
        return $fieldValues;
    }
}