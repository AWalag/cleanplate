<?php

require_once 'AppController.php';

session_start();

class ProfileController extends AppController{
    const MAX_FILE_SIZE = 1024 * 1024;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    public const UPLOAD_DIRECTORY = '/../images/uploads/avatars/';

    private $messages = [];

    public function profilePhotoChange(){
        $userRepository = new UserRepository();

        $errorMessages = [];
        $fileError = "";
        if(!$this->isPost() or !is_uploaded_file($_FILES['profilePhoto']['tmp_name'])) {
            array_push($errorMessages, ['avatarE' => ["No file selected"]]);
        }

        else if($this->validate($_FILES['profilePhoto'], $fileError) == false){
            array_push($errorMessages, ['avatarE' => [$fileError]]);
        }

        if(count($errorMessages) > 0){
            $this->render('profilepage', $errorMessages);
            return;
        }

        $shortType = $_FILES['profilePhoto']['type'];
        $shortType = explode('/', $shortType)[1];
        $fileName = $_SESSION['username'].$_SESSION['user_id'].".".$shortType;
        if($fileName != "none.png") {
            move_uploaded_file(
                $_FILES['profilePhoto']['tmp_name'],
                dirname(__DIR__) . self::UPLOAD_DIRECTORY . $fileName
            );
        }

        $userRepository->updateUserAvatar($_SESSION['user_id'], $fileName);
        $_SESSION['avatar'] = $fileName;

        $this->render('profilepage');
    }

    public function profileInfoChange(){
        $userRepository = new UserRepository();

        if(isset($_POST["username"]) and !empty($_POST["username"])) {
            $username = $_POST["username"];
            $username = htmlentities($username, ENT_QUOTES);

            //Username check
            $errorMessages = [];
            if((strlen($username) < 5) || (strlen($username) > 24)) {
                array_push($errorMessages, ['userE' => ["Username should have between 5 - 24 characters"]]);
            }
            else if(ctype_alnum($username) == false){
                array_push($errorMessages, ['userE' => ["Username should only consist of english characters or numbers"]]);
            }
            else if($userRepository->checkIfUserExists($username)){
                array_push($errorMessages, ['userE' => ["Username already taken"]]);
            }

            if(count($errorMessages) > 0){
                $this->render('profilepage', $errorMessages);
                return;
            }
            //---------------

            $userRepository->updateUsername($_SESSION["user_id"], $username);
            $_SESSION["username"] = $_POST["username"];
            $_SESSION["avatar"] = $userRepository->getUserAvatar($_SESSION["user_id"]);

        }

        if(isset($_POST["food-type"])) {
            $userRepository->updateFoodType($_SESSION['user_id'], $_POST["food-type"]);
            $_SESSION["food_type"] = $_POST["food-type"];
        }

        $this->render('profilepage');
    }

    private function validate(array $file, string &$error) : bool
    {
        if($file['size'] > self::MAX_FILE_SIZE){
            $error = "File is to large. Max size 1 mb";
            return false;
        }

        if(!isset($file['type']) or !in_array($file['type'], self::SUPPORTED_TYPES)){
            $error = "Only png and jpeg file formats allowed";
            return false;
        }
        return true;
    }
}