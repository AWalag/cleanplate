<?php

require_once 'AppController.php';

class DefaultController extends AppController{
    
    public function index(){
        $this->render('login');
    }

    public function register(){
        $this->render('register');
    }

    public function managePage(){
        $this->render('managepage');
    }

    public function recipesPage(){
        $this->render('recipespage');
    }

    public function groceryPage(){
        $this->render('grocerypage');
    }

    public function profilePage(){
        $this->render('profilepage');
    }

    public function recipeInstructionPage(){
        $this->render('recipeinstructionpage');
    }

    public function favoritePage(){
        $this->render('favoritepage');
    }
}