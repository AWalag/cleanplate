<?php

require_once 'AppController.php';
require_once __DIR__.'/../repository/CartRepository.php';
require_once __DIR__.'/../repository/RecipeRepository.php';

class CartController extends AppController{
    public function addToCart(){
        var_dump($_SERVER["CONTENT_TYPE"]);
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

        if($contentType === "application/json"){
            $content = trim(file_get_contents("php://input"));
            $decoded = json_decode($content, true);

            $cartRepository = new CartRepository();
            $cartRepository->addRecipeToCart($decoded['recipeId']);
        }
    }

    public function clearCart(){
        $cartRepository = new CartRepository();
        $cartRepository->clearCart();
    }

    public function getUserCart(){
        $cartRepository = new CartRepository();
        $userRecipesIds = $cartRepository->getUserRecipesId();

        $recipeRepository = new RecipeRepository();
        $productsList = [];
        $productsListPtr = 0;
        foreach ($userRecipesIds as $id) {
            $products = $recipeRepository->getProductsByRecipeId($id);
            $len = strlen($products);

            $productsSplit = [];
            $start = 0;
            $field = 0;

            for($i = 0; $i < $len; $i++){
                if($products[$i] == '[')
                    $start = $i + 1;
                if($products[$i] == ']'){
                    $productsSplit[$field] = substr($products, $start, $i - $start);
                    $field++;
                }
            }

            $arrSize = sizeof($productsSplit);
            for ($i = 0; $i < $arrSize; $i+=3){
                $productModel = new Product(
                    $productsSplit[$i],
                    $productsSplit[$i+1],
                    $productsSplit[$i+2]
                );
                $productsList[$productsListPtr] = $productModel;
                $productsListPtr++;
            }
        }
        header('Content-Type: application/json');
        http_response_code(200);

        echo json_encode($productsList);
    }
}