<?php

class User
{
    private $username;
    private $email;
    private $password;
    private $passwordSalt;
    private $name;
    private $surname;
    private $userID;


    public function __construct(string $username, string $email, string $password, string $passwordSalt, string $name, string $surname, int $userID){
        $this->username = $username;
        $this->email = $email;
        $this->password = $password;
        $this->passwordSalt = $passwordSalt;
        $this->name = $name;
        $this->surname = $surname;
        $this->userID = $userID;
    }

    public function getEmail(): string{
        return $this->email;
    }

    public function setEmail(string $email){
        $this->email = $email;
    }

    public function getPassword(): string{
        return $this->password;
    }

    public function setPassword(string $password){
        $this->password = $password;
    }

    public function getPasswordSalt(): string
    {
        return $this->passwordSalt;
    }

    public function getName(): string{
        return $this->name;
    }

    public function setName(string $name){
        $this->name = $name;
    }

    public function getSurname(): string{
        return $this->surname;
    }

    public function setSurname(string $surname){
        $this->surname = $surname;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username)
    {
        $this->username = $username;
    }

    public function getFoodType(): int
    {
        return $this->foodType;
    }

    public function setFoodType(int $foodType): void
    {
        $this->foodType = $foodType;
    }

    public function getUserID(): int
    {
        return $this->userID;
    }
}