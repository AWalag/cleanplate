<?php

class PreviewRecipe
{
    private $name;
    private $photo;
    private $chef_id;
    private $foodType_id;

    public function __construct($name, $photo, $chef_id, $foodType_id)
    {
        $this->name = $name;
        $this->photo = $photo;
        $this->chef_id = $chef_id;
        $this->foodType_id = $foodType_id;
    }

    function getName()
    {
        return $this->name;
    }

    public function getPhoto()
    {
        return $this->photo;
    }

    public function getChefId()
    {
        return $this->chef_id;
    }

    public function getFoodTypeId()
    {
        return $this->foodType_id;
    }
}