<?php

class Product
{
    public $name;
    public  $amount;
    public  $unit;

    public function __construct($name, $amount, $unit)
    {
        $this->name = $name;
        $this->amount = $amount;
        $this->unit = $unit;
    }
}