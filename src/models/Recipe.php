<?php
require_once "PreviewRecipe.php";

class Recipe extends PreviewRecipe
{
    private $recipeDescription;
    private $products;

    public function __construct($name, $photo, $chef_id, $foodType_id, $recipeDescription, $products)
    {
        parent :: __construct($name, $photo, $chef_id, $foodType_id);
        $this->recipeDescription = $recipeDescription;
        $this->products = $products;
    }


    public function getRecipeDescription()
    {
        return $this->recipeDescription;
    }

    public function getProducts()
    {
        return $this->products;
    }
}