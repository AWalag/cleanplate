<?php

class Chef
{
    private $first_name;
    private $second_name;

    public function __construct($first_name, $second_name)
    {
        $this->first_name = $first_name;
        $this->second_name = $second_name;
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function getSecondName()
    {
        return $this->second_name;
    }
}