<?php

require_once 'Repository.php';

session_start();

class CartRepository extends Repository
{
    public function addRecipeToCart(int $recipeId){
        $stmt = $this->database->connect()->prepare('
        INSERT INTO public.carts (user_id, recipe_id) 
        VALUES (:user_id, :recipe_id) 
        ');
        $stmt->bindParam(":user_id", $_SESSION['user_id'], PDO::PARAM_INT);
        $stmt->bindParam(":recipe_id", $recipeId, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function clearCart(){
        $stmt = $this->database->connect()->prepare('
        DELETE FROM public.carts WHERE user_id = :user_id
        ');
        $stmt->bindParam(":user_id", $_SESSION['user_id'], PDO::PARAM_INT);
        $stmt->execute();
    }

    public function getUserRecipesId() : array{
        $stmt = $this->database->connect()->prepare('
        SELECT recipe_id FROM public.carts WHERE user_id = :user_id
        ');
        $stmt->bindParam(":user_id", $_SESSION['user_id'], PDO::PARAM_INT);
        $stmt->execute();

        $recipeIds = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $recipeClearIds = [];
        foreach ($recipeIds as $Id){
            array_push($recipeClearIds, $Id['recipe_id']);
        }

        return $recipeClearIds;
    }
}