<?php

require_once 'Repository.php';

class FoodTypeRepository extends Repository{
    public function getFoodTypeId(string $food_type){
        $stmt = $this->database->connect()->prepare('
            SELECT food_type_id FROM public.food_types WHERE food_type_name = :food_type_name
        ');

        $stmt->bindParam(":food_type_name", $food_type, PDO::PARAM_STR);
        $stmt->execute();

        $exists = $stmt->fetch(PDO::FETCH_ASSOC);
        if($exists == false)
            return null;
        return $exists["food_type_id"];
    }
}