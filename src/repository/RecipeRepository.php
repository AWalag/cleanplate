<?php

session_start();

require_once 'Repository.php';
require_once __DIR__.'/../models/Recipe.php';
require_once __DIR__.'/../models/Product.php';
require_once __DIR__.'/../models/PreviewRecipe.php';
require_once __DIR__.'/../repository/FoodTypeRepository.php';
require_once __DIR__.'/../repository/ChefRepository.php';

class RecipeRepository extends Repository{
    public function createRecipe($valueFields){
        $foodType = new FoodTypeRepository();
        $chef = new ChefRepository();

        $stmt = $this->database->connect()->prepare('
            INSERT INTO public.recipes (recipe_name, recipe_description, food_type_id, chef_id, photo, products) 
            VALUES (:recipe_name, :recipe_description, :food_type_id, :chef_id, :photo, :products) 
        ');
        $valueFields[0] = strtolower($valueFields[0]);
        $stmt->bindParam(':recipe_name', $valueFields[0], PDO::PARAM_STR);
        $stmt->bindParam(':recipe_description', $valueFields[2], PDO::PARAM_STR);

        $foodTypeId = $foodType->getFoodTypeId($valueFields[4]);
        $stmt->bindParam(':food_type_id', $foodTypeId, PDO::PARAM_INT);

        $chefId = $chef->getChefId($valueFields[1]);
        if($chefId == null){
            $chef->addChef($valueFields[1]);
            $chefId = $chef->getChefId($valueFields[1]);
        }
        $stmt->bindParam(':chef_id', $chefId, PDO::PARAM_INT);

        $stmt->bindParam(':products', $valueFields[3], PDO::PARAM_STR);
        $stmt->bindParam(':photo', $valueFields[5], PDO::PARAM_STR);

        $stmt->execute();
    }

    public function getImageById(int $dishId){
        $stmt = $this->database->connect()->prepare('
            SELECT photo FROM public.recipes WHERE recipe_id = :recipe_id
        ');
        $stmt->bindParam(':recipe_id', $dishId, PDO::PARAM_INT);
        $stmt->execute();

        $recipePhoto = $stmt->fetch();
        if($recipePhoto == false){
            return null;
        }

        return $recipePhoto['photo'];
    }


    public function getRecipeId(string $recipeName){
        $stmt = $this->database->connect()->prepare('
            SELECT recipe_id FROM public.recipes WHERE recipe_name = :recipe_name
        ');
        $recipeName = strtolower($recipeName);
        $stmt->bindParam(':recipe_name', $recipeName, PDO::PARAM_STR);
        $stmt->execute();

        $recipeId = $stmt->fetch();
        if($recipeId == false){
            return null;
        }

        return $recipeId['recipe_id'];
    }

    public function checkIfRecipeExists(string $recipeName) : bool{
        $stmt = $this->database->connect()->prepare('
            SELECT recipe_id FROM public.recipes WHERE recipe_name = :recipe_name
        ');
        $recipeName = strtolower($recipeName);
        $stmt->bindParam(':recipe_name', $recipeName, PDO::PARAM_STR);
        $stmt->execute();

        $recipeId = $stmt->fetch();
        if($recipeId == false){
            return false;
        }

        return true;
    }

    public function deleteRecipe(string $recipeName) {
        $stmt = $this->database->connect()->prepare('
            DELETE FROM public.recipes WHERE recipe_name = :recipe_name
        ');
        $recipeName = strtolower($recipeName);
        $stmt->bindParam(':recipe_name', $recipeName, PDO::PARAM_STR);
        $stmt->execute();
    }

    public function getRecipesPrevious(){
        $userFoodType = $_SESSION['food_type'];
        $foodTypeRepository = new FoodTypeRepository();

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.recipes
        ');
        $stmt->execute();

        $recipeModels = [];
        $recipe = $stmt->fetch();
        while ($recipe != false){
            $userFoodTypeId = $foodTypeRepository->getFoodTypeId($userFoodType);
            $recipeFoodTypeId = $recipe['food_type_id'];
            if(($userFoodTypeId === 1 and $recipeFoodTypeId != 4) or ($userFoodTypeId === 2 and $recipeFoodTypeId === 2) or
                ($userFoodTypeId === 3 and ($recipeFoodTypeId == 3 or $recipeFoodTypeId == 2)) or ($userFoodTypeId === 4)) {
                $recipeModel = new PreviewRecipe(
                    $recipe['recipe_name'],
                    $recipe['photo'],
                    $recipe['chef_id'],
                    $recipe['food_type_id']
                );
                array_push($recipeModels, $recipeModel);
            }
            $recipe = $stmt->fetch();
        }

        return $recipeModels;
    }

    public function getRecipesPreviousFav(){
        $userId = $_SESSION['user_id'];
        $stmt = $this->database->connect()->prepare('
                SELECT recipe_id FROM public.favorites WHERE user_id = :user_id
        ');
        $stmt->bindParam(':user_id', $userId, PDO::PARAM_INT);
        $stmt->execute();

        $recipeModels = [];
        $recipe_id = $stmt->fetch();
        while ($recipe_id != false)
        {
            $stmtR = $this->database->connect()->prepare('
            SELECT * FROM public.recipes WHERE recipe_id = :recipe_id
            ');
            $stmtR->bindParam(':recipe_id', $recipe_id['recipe_id'], PDO::PARAM_INT);
            $stmtR->execute();

            $recipe = $stmtR->fetch();
            $recipeModel = new PreviewRecipe(
                $recipe['recipe_name'],
                $recipe['photo'],
                $recipe['chef_id'],
                $recipe['food_type_id']
            );
            array_push($recipeModels, $recipeModel);

            $recipe_id = $stmt->fetch();
        }
        return $recipeModels;
    }

    public function getFullRecipe(string $name) : Recipe{
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.recipes WHERE recipe_name = :recipe_name;
        ');
        $stmt->bindParam(':recipe_name', $name, PDO::PARAM_INT);
        $stmt->execute();

        $recipe = $stmt->fetch();
        return new Recipe(
            $recipe['recipe_name'],
            $recipe['photo'],
            $recipe['chef_id'],
            $recipe['food_type_id'],
            $recipe['recipe_description'],
            $recipe['products']
        );
    }

    public function getFullRecipeById(int $dishId) : Recipe{
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.recipes WHERE recipe_id = :recipe_id;
        ');
        $stmt->bindParam(':recipe_id', $dishId, PDO::PARAM_INT);
        $stmt->execute();

        $recipe = $stmt->fetch();
        return new Recipe(
            $recipe['recipe_name'],
            $recipe['photo'],
            $recipe['chef_id'],
            $recipe['food_type_id'],
            $recipe['recipe_description'],
            $recipe['products']
        );
    }

    public function getProductsByRecipeId(int $recipeId){
        $stmt = $this->database->connect()->prepare('
            SELECT products FROM public.recipes WHERE recipe_id = :recipe_id;
        ');

        $stmt->bindParam(':recipe_id', $recipeId, PDO::PARAM_INT);
        $stmt->execute();

        $recipe = $stmt->fetch();
        return $recipe['products'];
    }
}