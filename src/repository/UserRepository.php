<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/User.php';

class UserRepository extends Repository{
    public function getUser(string $username) : ?User
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.users WHERE username = :username
        ');
        $stmt->bindParam(':username', $username, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false){
            throw new Exception("No user matches login data");
        }

        return new User(
            $user['username'],
            $user['user_email'],
            $user['pass_hash'],
            $user['pass_salt'],
            $user['first_name'],
            $user['second_name'],
            (int)$user['user_id']
        );
    }

    public function checkIfUserExists(string $username) :  bool{
        $stmt = $this->database->connect()->prepare('
            SELECT user_id FROM public.users WHERE username = :username
        ');
        $stmt->bindParam(':username', $username, PDO::PARAM_STR);
        $stmt->execute();

        $exists = $stmt->fetch(PDO::FETCH_ASSOC);

        if($exists == false)
            return false;
        return true;
    }

    public function checkIfUserIsAnAdmin(int $userId) : bool{
        $stmt = $this->database->connect()->prepare('
            SELECT admin_id FROM public.admins WHERE user_id = :user_id
        ');

        $stmt->bindParam(':user_id', $userId, PDO::PARAM_INT);
        $stmt->execute();

        $exists = $stmt->fetch(PDO::FETCH_ASSOC);

        if($exists == false)
            return false;
        return true;
    }

    public function getUserAvatar(int $userId) : string{
        $stmt = $this->database->connect()->prepare('
            SELECT avatar_image_str FROM public.user_avatars WHERE user_id = :user_id
        ');

        $stmt->bindParam(':user_id', $userId, PDO::PARAM_INT);
        $stmt->execute();

        $avatar = $stmt->fetch(PDO::FETCH_ASSOC);

        if($avatar == false){
            throw new Exception("No avatar matches user data");
        }

        return $avatar["avatar_image_str"];
    }

    public function updateUserAvatar(int $userId, string $avatarStr){
        $stmt = $this->database->connect()->prepare('
            UPDATE public.user_avatars SET avatar_image_str = :image_str WHERE user_id = :user_id
        ');

        $stmt->bindParam(':image_str', $avatarStr, PDO::PARAM_INT);
        $stmt->bindParam(':user_id', $userId, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function updateUsername(int $userId, string $newUsername){
        //Update username in db
        $stmt = $this->database->connect()->prepare('
            UPDATE public.users SET username = :username WHERE user_id = :user_id
        ');

        $stmt->bindParam(':username', $newUsername, PDO::PARAM_INT);
        $stmt->bindParam(':user_id', $userId, PDO::PARAM_INT);
        $stmt->execute();

        //Update avatar name in db
        $stmt = $this->database->connect()->prepare('
            SELECT avatar_image_str FROM public.user_avatars WHERE user_id = :user_id
        ');

        $stmt->bindParam(':user_id', $userId, PDO::PARAM_INT);
        $stmt->execute();

        $avatar = $stmt->fetch(PDO::FETCH_ASSOC);
        if($avatar == false){
            throw new Exception("No avatar matches user data");
        }
        $type = explode(".", $avatar['avatar_image_str'])[1];

        $path = dirname(__DIR__).ProfileController::UPLOAD_DIRECTORY;
        rename($path.$avatar['avatar_image_str'], $path.$newUsername.$userId.".".$type);

        $stmt = $this->database->connect()->prepare('
            UPDATE public.user_avatars SET avatar_image_str = :image_str WHERE user_id = :user_id
        ');

        $newFileName = $newUsername.$userId.".".$type;
        $stmt->bindParam(':image_str', $newFileName, PDO::PARAM_INT);
        $stmt->bindParam(':user_id', $userId, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function updateFoodType(int $userId, string $foodType){
        $stmt = $this->database->connect()->prepare('
            UPDATE users_food_types SET food_type_id = (SELECT food_type_id FROM food_types WHERE food_type_name = :food_type_name)
            WHERE user_id = :user_id
        ');

        $stmt->bindParam(':food_type_name', $foodType, PDO::PARAM_STR);
        $stmt->bindParam(':user_id', $userId, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function checkIfEmailExists(string $email) :  bool{
        $stmt = $this->database->connect()->prepare('
            SELECT user_id FROM public.users WHERE user_email = :email
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $exists = $stmt->fetch(PDO::FETCH_ASSOC);
        if($exists == false)
            return false;
        return true;
    }

    public function getUserId(string $username)
    {
        $stmt = $this->database->connect()->prepare('
            SELECT user_id FROM public.users WHERE username = :username
        ');
        $stmt->bindParam(':username', $username, PDO::PARAM_STR);
        $stmt->execute();

        $userId = $stmt->fetch(PDO::FETCH_ASSOC);
        if($userId == false){
            throw new Exception("No user matches login data");
        }
        return $userId['user_id'];
    }

    public function getUserFoodType(int $userId) : string{
        $stmt = $this->database->connect()->prepare('
            SELECT food_type_name FROM public.food_types WHERE food_type_id = (SELECT food_type_id FROM public.users_food_types WHERE user_id = :user_id)
        ');
        $stmt->bindParam(':user_id', $userId, PDO::PARAM_INT);
        $stmt->execute();

        $foodTypeName = $stmt->fetch(PDO::FETCH_ASSOC);
        if($foodTypeName == false){
            throw new Exception("No user matches login data");
        }
        return $foodTypeName['food_type_name'];
    }

    public function createUser(string $username, string $name, string $surname, string $email, string $password, string $foodType){
        $passwordSalt = $this->passwordSaltGenerator(8);
        $password = $passwordSalt.$password;
        $password_hash = password_hash($password, PASSWORD_DEFAULT);

        $stmt = $this->database->connect()->prepare('
            INSERT INTO public.users (username, pass_hash, pass_salt, first_name, second_name, user_email) 
            VALUES (:username, :pass_hash, :pass_salt, :first_name, :second_name, :user_email) 
        ');
        $stmt->bindParam(':username', $username, PDO::PARAM_STR);
        $stmt->bindParam(':pass_hash', $password_hash, PDO::PARAM_STR);
        $stmt->bindParam(':pass_salt', $passwordSalt, PDO::PARAM_STR);
        $stmt->bindParam(':first_name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':second_name', $surname, PDO::PARAM_STR);
        $stmt->bindParam(':user_email', $email, PDO::PARAM_STR);

        $stmt->execute();

        $stmt = $this->database->connect()->prepare('
            INSERT INTO public.user_avatars (user_id, avatar_image_str) 
            VALUES (:user_id, :default_avatar)
            ');

        $user_id = $this->getUserId($username);
        $default_avatar = "none.png";
        $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $stmt->bindParam(':default_avatar', $default_avatar, PDO::PARAM_STR);

        $stmt->execute();

        //Add food type
        $stmt = $this->database->connect()->prepare('
            SELECT food_type_id FROM public.food_types WHERE food_type_name = :food_type_name
        ');
        $stmt->bindParam(':food_type_name', $foodType, PDO::PARAM_STR);
        $stmt->execute();

        $foodType_id = $stmt->fetch(PDO::FETCH_ASSOC);

        $stmt = $this->database->connect()->prepare('
            INSERT INTO public.users_food_types (user_id, food_type_id) VALUES (:user_id, :food_type_id) 
        ');

        $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $stmt->bindParam(':food_type_id', $foodType_id["food_type_id"], PDO::PARAM_INT);

        $stmt->execute();
    }

    private function passwordSaltGenerator(int $len) : String{
        $charSet = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charSetLen = strlen($charSet);
        $salt = '';

        for($i = 0; $i < $len; $i++){
            $salt .= $charSet[rand(0, $charSetLen - 1)];
        }
        return $salt;
    }
}