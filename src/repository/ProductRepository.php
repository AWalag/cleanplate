<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Product.php';

class ProductRepository extends Repository{
    public function createProduct(string $productName, $valueFields){
        $stmt = $this->database->connect()->prepare('
            INSERT INTO public.products (product_name, kcal, fats, protein, sugar, carbs) 
            VALUES (:product_name, :kcal, :fats, :protein, :sugar, :carbs) 
        ');
        $stmt->bindParam(':product_name', $productName, PDO::PARAM_STR);
        $stmt->bindParam(':kcal', $valueFields[0], PDO::PARAM_INT);
        $stmt->bindParam(':fats', $valueFields[1], PDO::PARAM_INT);
        $stmt->bindParam(':protein', $valueFields[2], PDO::PARAM_INT);
        $stmt->bindParam(':sugar', $valueFields[3], PDO::PARAM_INT);
        $stmt->bindParam(':carbs', $valueFields[4], PDO::PARAM_INT);

        $stmt->execute();
    }

    public function updateProduct(string $productName, $valueFields){
        $stmt = $this->database->connect()->prepare('
            UPDATE public.products SET kcal = :kcal, fats = :fats, protein = :protein, sugar = :sugar, carbs = :carbs 
            WHERE product_name = :product_name
        ');
        $stmt->bindParam(':product_name', $productName, PDO::PARAM_STR);
        $stmt->bindParam(':kcal', $valueFields[0], PDO::PARAM_INT);
        $stmt->bindParam(':fats', $valueFields[1], PDO::PARAM_INT);
        $stmt->bindParam(':protein', $valueFields[2], PDO::PARAM_INT);
        $stmt->bindParam(':sugar', $valueFields[3], PDO::PARAM_INT);
        $stmt->bindParam(':carbs', $valueFields[4], PDO::PARAM_INT);

        $stmt->execute();
    }

    public function deleteProduct(string $productName){
        $stmt = $this->database->connect()->prepare('
            DELETE FROM public.products WHERE product_name = :product_name
        ');
        $stmt->bindParam(':product_name', $productName, PDO::PARAM_STR);

        $stmt->execute();
    }

    public function checkIfProductExists(string $productName) :  bool{
        $stmt = $this->database->connect()->prepare('
            SELECT product_id FROM public.products WHERE product_name = :product_name
        ');
        $stmt->bindParam(':product_name', $productName, PDO::PARAM_STR);
        $stmt->execute();

        $exists = $stmt->fetch(PDO::FETCH_ASSOC);
        if($exists == false)
            return false;
        return true;
    }

    public function getProduct(string $productName) : Product{
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.products WHERE product_name = :product_name
        ');
        $stmt->bindParam(':product_name', $productName, PDO::PARAM_STR);
        $stmt->execute();

        $product = $stmt->fetch(PDO::FETCH_ASSOC);

        if($product == false){
            throw new Exception("No product matches product name");
        }

        return new User(
            $product['product_name'],
            $product['kcal'],
            $product['fats'],
            $product['protein'],
            $product['sugar'],
            $product['carbs']
        );
    }
}