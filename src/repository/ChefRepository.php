<?php

require_once 'Repository.php';

class ChefRepository extends Repository{
    public function getChefId(string $chef_name_surname){
        $stmt = $this->database->connect()->prepare('
            SELECT cheff_id FROM public.chefs WHERE first_name = :first_name AND second_name = :second_name
        ');

        $names = explode(" ", $chef_name_surname);
        $names[0] = strtolower($names[0]);
        $names[1] = strtolower($names[1]);

        $stmt->bindParam(":first_name", $names[0], PDO::PARAM_STR);
        $stmt->bindParam(":second_name", $names[1], PDO::PARAM_STR);
        $stmt->execute();

        $exists = $stmt->fetch(PDO::FETCH_ASSOC);
        if($exists == false)
            return null;
        return $exists["cheff_id"];
    }

    public function addChef(string $chef_name_surname){
        $stmt = $this->database->connect()->prepare('
            INSERT INTO public.chefs (first_name, second_name) 
            VALUES (:first_name, :second_name) 
        ');

        $names = explode(" ", $chef_name_surname);
        $names[0] = strtolower($names[0]);
        $names[1] = strtolower($names[1]);

        $stmt->bindParam(":first_name", $names[0], PDO::PARAM_STR);
        $stmt->bindParam(":second_name", $names[1], PDO::PARAM_STR);
        $stmt->execute();
    }

    public function getChefFromId(int $chef_id){
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.chefs WHERE cheff_id = :chef_id
        ');

        $stmt->bindParam(":chef_id", $chef_id, PDO::PARAM_INT);
        $stmt->execute();

        $chef = $stmt->fetch(PDO::FETCH_ASSOC);
        if($chef == false)
            return null;

        $firstName = $chef['first_name'];
        $firstName[0] = strtoupper($firstName[0]);

        $second_name = $chef['second_name'];
        $second_name[0] = strtoupper($second_name[0]);

        return $firstName.' '.$second_name;
    }
}