<?php

require_once 'Repository.php';

session_start();

class FavoriteRepository extends Repository
{
    public function addRecipeToFavorite(int $recipeId){
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.favorites WHERE user_id = :user_id AND recipe_id = :recipe_id
        ');
        $stmt->bindParam(":user_id", $_SESSION['user_id'], PDO::PARAM_INT);
        $stmt->bindParam(":recipe_id", $recipeId, PDO::PARAM_INT);
        $stmt->execute();

        if($stmt->fetch() == false) {
            $stmt = $this->database->connect()->prepare('
            INSERT INTO public.favorites (user_id, recipe_id) 
            VALUES (:user_id, :recipe_id) 
        ');
            $stmt->bindParam(":user_id", $_SESSION['user_id'], PDO::PARAM_INT);
            $stmt->bindParam(":recipe_id", $recipeId, PDO::PARAM_INT);
            $stmt->execute();
        }
    }
}