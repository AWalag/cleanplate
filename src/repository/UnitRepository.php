<?php

require_once 'Repository.php';

class UnitRepository extends Repository
{
    public function getUnitList()
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.units
        ');

        $stmt->execute();

        $unitList = [];
        $unitListDB = $stmt->fetch(PDO::FETCH_ASSOC);
        while ($unitListDB != false) {
            array_push($unitList, $unitListDB['unit_name']);
            $unitListDB = $stmt->fetch(PDO::FETCH_ASSOC);
        }

        return $unitList;
    }
}