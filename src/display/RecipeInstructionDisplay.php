<?php

require_once __DIR__.'/../models/PreviewRecipe.php';
require_once __DIR__.'/../repository/ChefRepository.php';
require_once __DIR__.'/../repository/RecipeRepository.php';

class RecipeInstructionDisplay{
    public function displayDishImage(int $dish_id){
        $recipeRepository = new RecipeRepository();
        $imgPath = '\'../../images/uploads/dishes/'.$recipeRepository->getImageById($dish_id).'\'';;
        echo '<img src='.$imgPath.'>';
    }

    public function displayDishInstruction(int $dish_id){
        $recipeRepository = new RecipeRepository();

        $ingredients = "";
        $fullRecipe = $recipeRepository->getFullRecipeById($dish_id);
        $productList = explode("\n", $fullRecipe->getProducts());

        foreach ($productList as $product){
            $product = str_replace('[','', $product);
            $product = str_replace(']',' ', $product);
            $product = "\t- ".$product;
            $ingredients = $ingredients.$product;
        }

        $instruction = "";
        $instructionList =  explode("\n", $fullRecipe->getRecipeDescription());
        foreach ($instructionList as $step){
            $instruction = $instruction."".$step."<br>";
        }

        echo ' <h5>INGREDIENTS</h5>
                    <pre>'.$ingredients.'</pre>

              <h5>INSTRUCTIONS</h5>
                    <text>'.$instruction.'</text>';
    }
}