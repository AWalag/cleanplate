<?php

require_once __DIR__.'/../models/PreviewRecipe.php';
require_once __DIR__.'/../repository/ChefRepository.php';
require_once __DIR__.'/../repository/RecipeRepository.php';

class RecipePreviewDisplay{
    public function displayRecipePreview(PreviewRecipe $recipePreviewModel, int $itemNr){
        $chefRepository = new ChefRepository();
        $recipeRepository = new RecipeRepository();

        $imagePath = '\'../../images/uploads/dishes/'.$recipePreviewModel->getPhoto().'\'';
        $recipe_name = $recipePreviewModel->getName();
        $recipe_name[0] = strtoupper($recipe_name[0]);
        echo '
                        <div class="item'.$itemNr.'" style="background-image: url('.$imagePath.'); background-size: cover; background-position: center";>
                        <a href="recipeInstructionPage?id='.$recipeRepository->getRecipeId($recipePreviewModel->getName()).'">
                            <div class="item-gradient">
                                <div class="recipe-name">'.$recipe_name.'</div>
                                <div class="chef-name">'.$chefRepository->getChefFromId($recipePreviewModel->getChefId()).'</div>
                            </div>
                        </a>
                        </div>
                        ';
    }

    public function displayRecipePreviewFav(PreviewRecipe $recipePreviewModel, int $itemNr){
        $recipeRepository = new RecipeRepository();

        $imagePath = '\'../../images/uploads/dishes/'.$recipePreviewModel->getPhoto().'\'';
        $recipe_name = $recipePreviewModel->getName();
        $recipe_name[0] = strtoupper($recipe_name[0]);


        echo '<div class="item'.$itemNr.'" id="grid-item" style="background-image: url('.$imagePath.'); background-size: cover; background-position: center";>
                     <a href="recipeInstructionPage?id='.$recipeRepository->getRecipeId($recipePreviewModel->getName()).'">
                    <i class="fas fa-heart"></i>
                    </a>
                </div>';
    }

    public function beginContainer(){
        echo '<div class="recipe-container">';
    }

    public function endContainer(){
        echo '</div>';
    }

}