<?php

require 'Routing.php';

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url($path, PHP_URL_PATH);

Routing::get('index', 'DefaultController');
Routing::get('register', 'DefaultController');
Routing::get('managePage', 'DefaultController');
Routing::get('recipesPage', 'DefaultController');
Routing::get('groceryPage', 'DefaultController');
Routing::get('profilePage', 'DefaultController');
Routing::get('recipeInstructionPage', 'DefaultController');
Routing::get('favoritePage', 'DefaultController');

Routing::get('loginSC', 'SecurityController');
Routing::get('logoutSC', 'SecurityController');
Routing::get('registerSC', 'SecurityController');

Routing::get('profileInfoChange', 'ProfileController');
Routing::get('profilePhotoChange', 'ProfileController');

Routing::get('manageProducts', 'ProductController');
Routing::get('manageRecipes', 'RecipeController');

Routing::get('addToFavorite', 'FavoriteController');
Routing::get('addToCart', 'CartController');
Routing::get('clearCart', 'CartController');
Routing::get('getUserCart', 'CartController');

Routing::run($path);