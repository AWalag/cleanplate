<?php
    session_start();

    if(isset($_SESSION['loggedIn']) && ($_SESSION['loggedIn'] == true)){
        header('Location: recipesPage');
        exit();
    }
?>

<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style_login.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>clean plate</title>
</head>

<body>
    <div class="logo">
        <img id="logo-bg" src="images/login/logo_bg.svg">
        <img id="logo-bgm" src="images/login/logo_bgM.svg">
        <img id="logo-fr" src="images/logo/logo.svg">
    </div>

    <div class="background">
        <div class="bg-image-box">
            <img src="images/login/bg_1.jpg">
        </div>
        <div class="bg-overlay-box">
            <img src="images/login/login_overlay.svg">
        </div>
    </div>
    
    <div class="login">
        <form action="loginSC" method="POST">
            <div class="loginMessage">
                <?php
                    if(isset($messages)){
                        foreach ($messages as $message){
                            echo $message.'<br/>';
                        }
                    }
                ?>
            </div>
            <div class="login-display">
                Login
            </div>
                username
            <input name="username" type="text" placeholder="username">
                password
            <input name="password" type="password" placeholder="password">
            <div class="login-buttons">
                <button type="submit" formaction="register">
                    register
                </button>
                <button type="submit">
                    login
                </button>
            </div>
            <div class="login-line">
            </div>
        </form>
    </div>
</body>