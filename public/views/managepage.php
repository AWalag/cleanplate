<?php
session_start();
if(!isset($_SESSION['loggedIn'])){
    header('Location: index');
    exit();
}
?>

<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style_left_menu.css">
    <link rel="stylesheet" type="text/css" href="public/css/style_mp.css">
    <script src="https://kit.fontawesome.com/5223fe35ab.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="../public/js/ValidateChef.js" defer></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>clean plate</title>
</head>

<body>
    <div class="main-container">
        <div class="menu">
            <div class="user-data">
                <a id="avatar-upload" href="profilePage">
                    <?php
                    echo "<img src='images/uploads/avatars/".$_SESSION['avatar']."'>";
                    ?>
                </a>
                <div class="user-data-info">
                    <?php
                    if(isset($_SESSION['admin']))
                        echo "<h4 style='color: gold'><b>".$_SESSION['username']."</b></h4>";
                    else
                        echo "<h4>".$_SESSION['username']."</h4>";
                    ?>
                    <h4>
                        <?php
                        $foodType = $_SESSION['food_type'];
                        $foodType[0] = strtoupper($foodType[0]);
                        echo "<h4 style='margin-top: -0.5em'>".$foodType."</h4>";
                        ?>
                    </h4>
                </div>
            </div>

            <div class="tabs">
                <ul>
                    <a href="recipesPage">
                        <li>
                            <i class="fas fa-book"></i>
                            <h7>Recipes</h7>
                        </li>
                    </a>
                    <a href="groceryPage">
                        <li>
                            <i class="fas fa-shopping-cart"></i>
                            <h7>Grocery list</h7>
                        </li>
                    </a>
                    <a href="favoritePage">
                        <li>
                            <i class="far fa-star"></i>
                            <h7>Favorite recpies</h7>
                        </li>
                    </a>
                    <?php
                    if(isset($_SESSION['admin']))
                        echo'
                        <a href="managePage">
                            <li   style="background-color: #F29F05; border-radius: 0px 0px 10px 0px;">
                                <i class="fas fa-edit"></i>
                                <h7>Manage</h7>
                            </li>
                        </a>
                        ';
                    ?>
                </ul>
            </div>

            <div class="logout">
                <a href="logoutSC">
                    <button>
                        logout
                    </button>
                </a>
            </div>
        </div>

        <main>
          <div class="add-recipe">
              <h2>Manage recipes</h2>
                <form action="manageRecipes" method="POST" ENCTYPE="multipart/form-data">
                    <h7>Manage operation</h7>
                    <select name="manage_operation">
                        <option disabled selected value style="display: none;"> select an operation </option>
                        <option>Add</option>
                        <option>Delete</option>
                    </select>
                    <div class="error-msg"> <?php if(isset($recipeOperationE)){ foreach ($recipeOperationE as $message){ echo $message.'<br/>';}} ?> </div>
                    <h7>Dish name</h7>
                    <input name="dish_name" type="text" placeholder="dish name">
                    <div class="error-msg"> <?php if(isset($dish_nameE)){ foreach ($dish_nameE as $message){ echo $message.'<br/>';}} ?> </div>
                    <h7>Chef name and surname</h7>
                    <input name="chef_name" type="text" placeholder="chef name and surname">
                    <div class="error-msg"> <?php if(isset($chef_nameE)){ foreach ($chef_nameE as $message){ echo $message.'<br/>';}} ?> </div>
                    <h7>Recipe</h7>
                    <textarea name="recipe" >recipe...</textarea>
                    <div class="error-msg"> <?php if(isset($recipeE)){ foreach ($recipeE as $message){ echo $message.'<br/>';}} ?> </div>
                    <h7>Products</h7>
                    <textarea name="products">[product name][amout][unit]</textarea>
                    <div class="error-msg"> <?php if(isset($productsE)){ foreach ($productsE as $message){ echo $message.'<br/>';}} ?> </div>
                    <h7>Add dish photo</h7>
                    <input name="dish_photo" type="file" style="height: 2em; border-radius: 0 0; border: 0; box-shadow: inset 0 0 0; margin-top: 0.5em">
                    <div class="error-msg"> <?php if(isset($photoE)){ foreach ($photoE as $message){ echo $message.'<br/>';}} ?> </div>
                    <h7>Select dish type</h7>
                    <select name="food_type">
                        <option disabled selected value style="display: none;"> select an option </option>
                        <option>pescetarian</option>
                        <option>vegan</option>
                        <option>vegetarian</option>
                        <option>everything</option>
                    </select>
                    <div class="error-msg"> <?php if(isset($foodTypeE)){ foreach ($foodTypeE as $message){ echo $message.'<br/>';}} ?> </div>
                    <button type="submit">
                        confirm
                    </button>
                </form>
		  </div>

            <div class="divisor"></div>

          <div class="add-product">
              <h2>Manage products</h2>
              <form action="manageProducts" method="POST">
                  <h7>Manage operation</h7>
                  <select name="manage_operation">
                      <option disabled selected value style="display: none;"> select an operation </option>
                      <option>Add</option>
                      <option>Update</option>
                      <option>Delete</option>
                  </select>
                  <div class="error-msg"> <?php if(isset($productOperationE)){ foreach ($productOperationE as $message){ echo $message.'<br/>';}} ?> </div>
                  <h7>Product name</h7>
                  <input name="product_name" type="text" placeholder="product name">
                  <div class="error-msg"> <?php if(isset($nameE)){ foreach ($nameE as $message){ echo $message.'<br/>';}} ?> </div>
                  <h7>Kilo calories</h7>
                  <input name="kcal" type="number" step="0.01">
                  <div class="error-msg"> <?php if(isset($kcalE)){ foreach ($kcalE as $message){ echo $message.'<br/>';}} ?> </div>
                  <h7>Fats</h7>
                  <input name="fats" type="number" step="0.01">
                  <div class="error-msg"> <?php if(isset($fatsE)){ foreach ($fatsE as $message){ echo $message.'<br/>';}} ?> </div>
                  <h7>Proteins</h7>
                  <input name="proteins" type="number" step="0.01">
                  <div class="error-msg"> <?php if(isset($proteinsE)){ foreach ($proteinsE as $message){ echo $message.'<br/>';}} ?> </div>
                  <h7>Sugars</h7>
                  <input name="sugars" type="number" step="0.01">
                  <div class="error-msg"> <?php if(isset($sugarsE)){ foreach ($sugarsE as $message){ echo $message.'<br/>';}} ?> </div>
                  <h7>Cabs</h7>
                  <input name="carbs" type="number" step="0.01">
                  <div class="error-msg"> <?php if(isset($carbsE)){ foreach ($carbsE as $message){ echo $message.'<br/>';}} ?> </div>
                  <button type="submit">
                      confirm
                  </button>
              </form>
          </div>
        </main>
    </div>
</body>