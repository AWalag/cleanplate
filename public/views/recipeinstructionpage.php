<?php
    session_start();
    if(!isset($_SESSION['loggedIn'])){
        header('Location: index');
        exit();
    }
?>

<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style_left_menu.css">
    <link rel="stylesheet" type="text/css" href="public/css/style_rip.css">
    <script src="https://kit.fontawesome.com/5223fe35ab.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="../public/js/RecipeOperations.js" defer></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>clean plate</title>
</head>

<body>
    <div class="main-container">
        <div class="menu">
            <div class="user-data">
                <a id="avatar-upload" href="profilePage">
                    <?php
                    echo "<img src='images/uploads/avatars/".$_SESSION['avatar']."'>";
                    ?>
                </a>
                <div class="user-data-info">
                    <?php
                    if(isset($_SESSION['admin']))
                        echo "<h4 style='color: gold'><b>".$_SESSION['username']."</b></h4>";
                    else
                        echo "<h4>".$_SESSION['username']."</h4>";
                    ?>
                    <h4>
                        <?php
                        $foodType = $_SESSION['food_type'];
                        $foodType[0] = strtoupper($foodType[0]);
                        echo "<h4 style='margin-top: -0.5em'>".$foodType."</h4>";
                        ?>
                    </h4>
                </div>
            </div>

            <div class="tabs">
                <ul>
                    <a href="recipesPage">
                        <li>
                            <i class="fas fa-book"></i>
                            <h7>Recipes</h7>
                        </li>
                    </a>
                    <a href="groceryPage">
                        <li>
                            <i class="fas fa-shopping-cart"></i>
                            <h7>Grocery list</h7>
                        </li>
                    </a>
                    <a href="favoritePage">
                        <li>
                            <i class="far fa-star"></i>
                            <h7>Favorite recpies</h7>
                        </li>
                    </a>
                    <?php
                    if(isset($_SESSION['admin']))
                        echo'
                        <a href="managePage">
                            <li>
                                <i class="fas fa-edit"></i>
                                <h7>Manage</h7>
                            </li>
                        </a>
                        ';
                    ?>
                </ul>
            </div>

            <div class="logout">
                <a href="logoutSC">
                    <button>
                        logout
                    </button>
                </a>
            </div>
        </div>

        <main>
            <div class="recipe-photo">
                <?php
                    require_once __DIR__.'/../../src/display/RecipeInstructionDisplay.php';
                    $recipeInstructionDisplay = new RecipeInstructionDisplay();
                    $recipeInstructionDisplay->displayDishImage($_GET['id']);
                ?>
            </div>

            <div class="separation-line">

            </div>
            <div class="main-recipe">
                <div class="recipe-text">
                    <?php
                        require_once __DIR__.'/../../src/display/RecipeInstructionDisplay.php';
                        $recipeInstructionDisplay = new RecipeInstructionDisplay();
                        $recipeInstructionDisplay->displayDishInstruction($_GET['id']);
                    ?>
                </div>
                <div class="recipe-actions">
                    <button name="cart">
                        <i class="fas fa-shopping-cart"></i>
                        <h7>Add ingredients to cart</h7>
                    </button>

                    <button name="fav">
                        <i class="fas fa-heart"></i>
                        <h7>Add to favorite</h7>
                    </button>
                </div>
            </div>
        </main>
    </div>
</body>