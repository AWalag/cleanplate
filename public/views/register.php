<?php
session_start();

if(isset($_SESSION['loggedIn']) && ($_SESSION['loggedIn'] == true)){
    header('Location: recipesPage');
    exit();
}
?>

<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style_reg.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register page</title>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body>
    <div class="register-container">
        <div class="logo">
            <img id="logo-fr" src="images/logo/logo.svg">
        </div>
       
        <div class="register-text">
            Rejestration
        </div>

        <main>
            <div class="form">
                <form id="register-form" action="registerSC" method="POST">
                    username
                    <input name="username" type="text" placeholder="username">
                    <div class="error-msg"> <?php if(isset($userE)){ foreach ($userE as $message){ echo $message.'<br/>';}} ?> </div>
                    name
                    <input name="name" type="text" placeholder="name">
                    <div class="error-msg"> <?php if(isset($nameE)){ foreach ($nameE as $message){ echo $message.'<br/>';}} ?> </div>
                    surname
                    <input name="surname" type="text" placeholder="surname">
                    <div class="error-msg"> <?php if(isset($surnameE)){ foreach ($surnameE as $message){ echo $message.'<br/>';}} ?> </div>
                    email
                    <input name="email" type="text" placeholder="email">
                    <div class="error-msg"> <?php if(isset($emailE)){ foreach ($emailE as $message){ echo $message.'<br/>';}} ?> </div>
                    password
                    <input name="password" type="password" placeholder="password">
                    <div class="error-msg"> <?php if(isset($passwordE)){ foreach ($passwordE as $message){ echo $message.'<br/>';}} ?> </div>
                    repeat password
                    <input name="repeat-password" type="password" placeholder="password">
                    <div class="error-msg"> <?php if(isset($passwordRE)){ foreach ($passwordRE as $message){ echo $message.'<br/>';}} ?> </div>
                    what food do you prefer?
                    <select name="food-type">
                        <option disabled selected value style="display: none;"> select an option </option>
                        <option>pescetarian</option>
                        <option>vegan</option>
                        <option>vegetarian</option>
                        <option>everything</option>
                    </select>
                    <div class="error-msg"> <?php if(isset($foodTypeE)){ foreach ($foodTypeE as $message){ echo $message.'<br/>';}} ?> </div>

                    <div class="g-recaptcha" data-sitekey="6LcdXu0dAAAAAPNZaji2xofYyWN3690b7OH-BdSz"></div>
                    <div class="error-msg"> <?php if(isset($captchaE)){ foreach ($captchaE as $message){ echo $message.'<br/>';}} ?> </div>

                    <button id="mobile-submit" type="submit">
                        Continue
                    </button>
                </form>
            </div>
            <div class="right-page">
                <p>
                    Did you know
                </p>
                <a>
                    Clean plate save you money. A large study found unhealthy diets full of processed foods cost $1.50 more a day than healthy ones full of whole foods. Also eating better could boost your energy, immunity system, overall well being and enhance your athletic performance.
                    <br><br><br><br>
                </a>
                
                <button id="wb-submit" type="submit" form="register-form">
                    continue
                </button>
            </div>
        </main>

    </div>
    <div class="footer">
    </div>
</body>