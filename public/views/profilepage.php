<?php
    session_start();
    if(!isset($_SESSION['loggedIn'])){
        header('Location: index');
        exit();
    }
?>

<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style_left_menu.css">
    <link rel="stylesheet" type="text/css" href="public/css/style_pp.css">
    <script src="https://kit.fontawesome.com/5223fe35ab.js" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>clean plate</title>
</head>

<body>
    <div class="main-container">
        <div class="menu">
            <div class="user-data">
                <a id="avatar-upload" href="profilePage">
                    <?php
                    echo "<img src='images/uploads/avatars/".$_SESSION['avatar']."'>";
                    ?>
                </a>
                <div class="user-data-info">
                    <?php
                    if(isset($_SESSION['admin']))
                        echo "<h4 style='color: gold'><b>".$_SESSION['username']."</b></h4>";
                    else
                        echo "<h4>".$_SESSION['username']."</h4>";
                    ?>
                    <h4>
                        <?php
                        $foodType = $_SESSION['food_type'];
                        $foodType[0] = strtoupper($foodType[0]);
                        echo "<h4 style='margin-top: -0.5em'>".$foodType."</h4>";
                        ?>
                    </h4>
                </div>
            </div>

            <div class="tabs">
                <ul>
                    <a href="recipesPage">
                        <li>
                            <i class="fas fa-book"></i>
                            <h7>Recipes</h7>
                        </li>
                    </a>
                    <a href="groceryPage">
                        <li>
                            <i class="fas fa-shopping-cart"></i>
                            <h7>Grocery list</h7>
                        </li>
                    </a>
                    <a href="favoritePage">
                        <li>
                            <i class="far fa-star"></i>
                            <h7>Favorite recpies</h7>
                        </li>
                    </a>
                    <?php
                    if(isset($_SESSION['admin']))
                        echo'
                        <a href="managePage">
                            <li>
                                <i class="fas fa-edit"></i>
                                <h7>Manage</h7>
                            </li>
                        </a>
                        ';
                    ?>
                </ul>
            </div>

            <div class="logout">
                <a href="logoutSC">
                    <button>
                        logout
                    </button>
                </a>
            </div>
        </div>

        <main>
            <div class="profile-container">
                <div class="photo-settings">
                    <form action="profilePhotoChange" method="POST" ENCTYPE="multipart/form-data">
                        <?php
                            echo "<img src='images/uploads/avatars/".$_SESSION['avatar']."'>";
                        ?>
                        <input type="file" id="selectedFile" style="display: none;" name="profilePhoto" />
                        <input type="button" value="Change photo" onclick="document.getElementById('selectedFile').click();" />
                        <button type="submit">
                            Send new photo
                        </button>
                        <div class="error-msg" style="margin-top: 0.5em"> <?php if(isset($avatarE)){ foreach ($avatarE as $message){ echo $message.'<br/>';}} ?> </div>
                    </form>
                </div>
                <div class="user-data-settings">
                    <form action="profileInfoChange" method="POST">
                        <h7>Username</h7>
                        <input name="username" type="text" placeholder="new username">
                        <div class="error-msg"> <?php if(isset($userE)){ foreach ($userE as $message){ echo $message.'<br/>';}} ?> </div>
                        <h7>preferred food</h7>
                        <select name="food-type">
                            <option disabled selected value style="display: none;"> change food type </option>
                            <option>pescetarian</option>
                            <option>vegan</option>
                            <option>vegetarian</option>
                            <option>everything</option>
                        </select>
                        <button type="submit">
                            Accept changes
                        </button>
                        <div class="logout">
                            <a href="logoutSC">
                                <button>
                                    logout
                                </button>
                            </a>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </main>
    </div>
</body>