<?php
session_start();
if(!isset($_SESSION['loggedIn'])){
    header('Location: index');
    exit();
}
?>

<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style_rp.css">
    <script src="https://kit.fontawesome.com/5223fe35ab.js" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>clean plate</title>
</head>

<body>
<div class="main-container">
    <div class="menu">
        <div class="user-data">
            <a id="avatar-upload" href="profilePage">
                <?php
                echo "<img src='images/uploads/avatars/".$_SESSION['avatar']."'>";
                ?>
            </a>
            <div class="user-data-info">
                <?php
                if(isset($_SESSION['admin']))
                    echo "<h4 style='color: gold'><b>".$_SESSION['username']."</b></h4>";
                else
                    echo "<h4>".$_SESSION['username']."</h4>";
                ?>
                <h4>
                    <?php
                    $foodType = $_SESSION['food_type'];
                    $foodType[0] = strtoupper($foodType[0]);
                    echo "<h4 style='margin-top: -0.5em'>".$foodType."</h4>";
                    ?>
                </h4>
            </div>
        </div>

        <div class="tabs">
            <ul>
                <a href="recipesPage">
                    <li style="background-color: #F29F05; border-radius: 0px 0px 10px 0px;">
                        <i class="fas fa-book"></i>
                        <h7>Recipes</h7>
                    </li>
                </a>
                <a href="groceryPage">
                    <li>
                        <i class="fas fa-shopping-cart"></i>
                        <h7>Grocery list</h7>
                    </li>
                </a>
                <a href="favoritePage">
                    <li>
                        <i class="far fa-star"></i>
                        <h7>Favorite recpies</h7>
                    </li>
                </a>
                <?php
                if(isset($_SESSION['admin']))
                    echo'
                        <a href="managePage">
                            <li>
                                <i class="fas fa-edit"></i>
                                <h7>Manage</h7>
                            </li>
                        </a>
                        ';
                ?>
            </ul>
        </div>

        <div class="logout">
            <a href="logoutSC">
                <button>
                    logout
                </button>
            </a>
        </div>
    </div>

        <main>
                <?php
                    require_once __DIR__.'/../../src/repository/RecipeRepository.php';
                    require_once __DIR__.'/../../src/display/RecipePreviewDisplay.php';

                    $recipeRepository = new RecipeRepository();
                    $recipePreviews = $recipeRepository->getRecipesPrevious();
                    $recipeDisplay = new RecipePreviewDisplay();

                    $itemCounter = 1;
                    foreach ($recipePreviews as $preview) {
                        if($itemCounter === 1) {
                            $recipeDisplay->beginContainer();
                        }

                        $recipeDisplay->displayRecipePreview($preview, $itemCounter);
                        if($itemCounter === 5) {
                            $recipeDisplay->endContainer();
                        }

                        $itemCounter = ($itemCounter++ % 5) + 1;
                    }
                ?>
        </main>
    </div>
</body>