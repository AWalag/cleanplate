const inputFav = document.querySelector('button[name="fav"]');
const inputCart = document.querySelector('button[name="cart"]');

let recipeID = getRecipeId();

inputFav.addEventListener("click", function (){
    const data = {recipeId: recipeID};

    fetch("/addToFavorite", {
        method: "POST",
        headers:{
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }).then(function (response)
    {
        alert('Added to favorite');
    });
});

inputCart.addEventListener("click", function (){
    const data = {recipeId: recipeID};

    fetch("/addToCart", {
        method: "POST",
        headers:{
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }).then(function (response)
    {
        alert('Added to cart');
    });
});

function getRecipeId(){
    if(document.location.toString().indexOf('?') !== -1) {
        let query = document.location.toString().replace(/^.*?\?/, '').replace(/#.*$/, '').split('&');
        let querySplit = query[0].split('=');
        return querySplit[1];
    }
    alert("Refresh the page");
    return null;
}