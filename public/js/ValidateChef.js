const input = document.querySelector('input[name="chef_name"]');

function hasNameAndSurname(chefNameSurname){
    let nameSeparator = chefNameSurname.split(" ");
    if(nameSeparator.length === 2)
        return true;
    return false;
}

function markValidation(element, condition){
    !condition ? element.classList.add('no-valid') : element.classList.remove('no-valid');
}

input.addEventListener('keyup', function () {
    setTimeout(function (){
        markValidation(input, hasNameAndSurname(input.value))
    }, 1000);
});