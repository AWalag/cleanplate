const inputClear = document.querySelector('button[class="clean-cart-operation"]');
const groceryListContainer = document.querySelector('div[class="ingredient-list"]');

createCart();

function createCart(){
    fetch("/getUserCart",
    ).then(function (response)
    {
        return response.json();
    }).then(function(ingredientList){
        loadGroceryList(ingredientList);
    });
}

function loadGroceryList(ingredientList){
    let ingredientMap = mapIngredients(ingredientList);
    let htmlCoded = getIngredientAsHTML(ingredientMap);

    const htmlNames = groceryListContainer.querySelector('div[class="ingredient-list-names"]');
    htmlNames.innerHTML = htmlCoded[0];

    const htmlCount = groceryListContainer.querySelector('div[class="ingredient-list-counts"]');
    htmlCount.innerHTML = htmlCoded[1];


    console.log(ingredientMap);
}

function mapIngredients(ingredientList){
    let ingredientMap = new Map();

    ingredientList.forEach(ingredient => {
        let multi = 1;
        if(ingredient['unit'] === "kg" || ingredient['unit'] === "l")
            multi = 1000;
        let unit = "x"
        if(ingredient['unit'] === "kg" || ingredient['unit'] === "g" )
            unit = "g"
        else if(ingredient['unit'] === "ml" || ingredient['unit'] === "l" )
            unit = "ml"


        if(ingredientMap.has(ingredient['name']) === false){
            ingredientMap.set(ingredient['name'], [parseInt(ingredient['amount']) * multi, unit]);
        }
        else{
            let currentAmount = ingredientMap.get(ingredient['name'])[0];
            ingredientMap.set(ingredient['name'], [currentAmount + parseInt(ingredient['amount']) * multi, unit]);
        }
    });

    return ingredientMap;
}

function getIngredientAsHTML(ingredientMap){
    let names = ingredientMap.keys();
    let finalHtmlNames = ""
    let finalHtmlCount = ""
    let size = ingredientMap.size;

    for(let i = 0; i < size; i++) {
        let key = names.next().value

        finalHtmlNames += "<h6>";
        finalHtmlNames += key;
        finalHtmlNames += "</h6>\n";

        finalHtmlCount += "<h6>";
        let amount = ingredientMap.get(key);
        let unit = amount[1];

        if(unit !== "x"){
            if(amount[0] < 1000){
                finalHtmlCount += amount[0] + " " + unit;
            }
            else{
                if(unit === "g")
                    unit = "kg";
                else
                    unit = "l";
                finalHtmlCount += amount[0] / 1000.0 + " " + unit;
            }
        }
        else{
            finalHtmlCount += amount[0] + " " + unit;
        }

        finalHtmlCount += "</h6>\n";
    }

    return [finalHtmlNames, finalHtmlCount];
}

inputClear.addEventListener("click", function (){
    fetch("/clearCart",).then(function (response)
    {
        groceryListContainer.innerHTML = "";
        alert('Cart cleared');
    });
});