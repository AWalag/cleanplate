CREATE TABLE foodType(
    foodType_id INT AUTO_INCREMENT,
	
    name varchar(255)  NOT NULL,
    
    CONSTRAINT PRIMARY KEY(foodType_id),
	UNIQUE(name)
    );

CREATE TABLE users(
    user_id INT AUTO_INCREMENT,
	user_foodType_id INT NOT NULL,
	
    username varchar(255)  NOT NULL,
    pass_hash varchar(255) NOT NULL,
    pass_salt varchar(255) NOT NULL,
    first_name varchar(255)  NOT NULL,
    second_name varchar(255)  NOT NULL,
	user_mail varchar(255) NOT NULL CHECK(user_mail LIKE('%@%.%')),
    
    CONSTRAINT PRIMARY KEY(user_id),
	UNIQUE(user_mail),
	CONSTRAINT
		FOREIGN KEY(user_foodType_id)
		REFERENCES foodType(foodType_id)
        ON DELETE CASCADE
    );
	
CREATE TABLE calendar(
	calendar_id INT AUTO_INCREMENT,
	user_id INT NOT NULL,
	
    CONSTRAINT PRIMARY KEY(calendar_id),
	CONSTRAINT
		FOREIGN KEY(user_id)
		REFERENCES users(user_id)
        ON DELETE CASCADE
);

CREATE TABLE unit(
	unit_id INT AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	
	CONSTRAINT PRIMARY KEY(unit_id)
);

CREATE TABLE product(
	product_id INT AUTO_INCREMENT,
	
	name varchar(255) NOT NULL,
	kcal INT NOT NULL,
	carbs INT NOT NULL,
	fats INT NOT NULL,
	protein INT NOT NULL,
	sugar INT NOT NULL,
	alternative_id INT,
	
	CONSTRAINT PRIMARY KEY(product_id),
	CONSTRAINT
		FOREIGN KEY(alternative_id)
		REFERENCES product(product_id)
        ON DELETE CASCADE
);

CREATE TABLE cart(
	cart_id INT AUTO_INCREMENT,
	user_id INT NOT NULL,
	
	CONSTRAINT PRIMARY KEY(cart_id),
	CONSTRAINT
		FOREIGN KEY(user_id)
		REFERENCES users(user_id)
        ON DELETE CASCADE
);

CREATE TABLE cheff(
	cheff_id INT AUTO_INCREMENT,
	first_name varchar(255)  NOT NULL,
	second_name varchar(255)  NOT NULL,
	
	CONSTRAINT PRIMARY KEY(cheff_id)
);

CREATE TABLE recipe(
	recipe_id INT AUTO_INCREMENT,
	cheff_id INT NOT NULL,
	recipie_description LONGTEXT NOT NULL,
	foodType_id INT NOT NULL,
	
	CONSTRAINT PRIMARY KEY(recipe_id),
	CONSTRAINT
		FOREIGN KEY(cheff_id)
		REFERENCES cheff(cheff_id)
        ON DELETE CASCADE,
	CONSTRAINT
		FOREIGN KEY(foodType_id)
		REFERENCES foodType(foodType_id)
        ON DELETE CASCADE
);

CREATE TABLE foodPack(
	foodPack_id INT AUTO_INCREMENT,
	
	cart_id INT,
	recipe_id INT,
	product_id INT NOT NULL,
	unit_id INT NOT NULL,
	amount INT NOT NULL,
	
	CONSTRAINT PRIMARY KEY(foodPack_id),
	
	CONSTRAINT
		FOREIGN KEY(unit_id)
		REFERENCES unit(unit_id)
        ON DELETE CASCADE,
		
	CONSTRAINT
		FOREIGN KEY(cart_id)
		REFERENCES cart(cart_id)
        ON DELETE CASCADE,
		
	CONSTRAINT
		FOREIGN KEY(recipe_id)
		REFERENCES recipe(recipe_id)
        ON DELETE CASCADE,
		
	CONSTRAINT
		FOREIGN KEY(product_id)
		REFERENCES product(product_id)
        ON DELETE CASCADE
);

CREATE TABLE calendarDay(
	calendarDay_id INT AUTO_INCREMENT,
	calendar_id INT NOT NULL,
	
	recipe_id INT NOT NULL,
	dayDate DATE NOT NULL,
	
	CONSTRAINT PRIMARY KEY(calendarDay_id),
	CONSTRAINT
		FOREIGN KEY(recipe_id)
		REFERENCES recipe(recipe_id)
        ON DELETE CASCADE,
	CONSTRAINT
		FOREIGN KEY(calendar_id)
		REFERENCES calendar(calendar_id)
        ON DELETE CASCADE
);